import HomePage from "./pages/HomePage";
import OrderDetailPage from "./pages/OrderDetail";

const routerList = [
    { path: "/", element: <HomePage /> },
    { path: "/detail",element: <OrderDetailPage /> }
]
export default routerList;