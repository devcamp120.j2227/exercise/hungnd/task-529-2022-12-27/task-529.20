import { BUTTON_DETAIL_HANDLER, BUTTON_QUANTITY_HANDLER } from "../constants/order.constant"

const onBtnQuantilyHandler = (data) => {
    return {
        type: BUTTON_QUANTITY_HANDLER,
        data: data
    }
}
const onBtnOrderDetail = (data) => {
    return {
        type: BUTTON_DETAIL_HANDLER,
        data: data
    }
}
export {
    onBtnOrderDetail,
    onBtnQuantilyHandler
}