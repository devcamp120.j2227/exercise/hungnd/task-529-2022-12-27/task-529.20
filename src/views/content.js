import routes from '../routes';
import { Routes, Route } from "react-router-dom";

const Content = () => {
    return (
        <div style={{ marginTop: "30px" }}>
            <Routes>
                {routes.map((router, index) => {
                    if (router.path) {
                        return <Route key={index} exact path={router.path} element={router.element}></Route>
                    }
                    else {
                        return null;
                    }

                })}
            </Routes>
        </div>
    )
}
export default Content;