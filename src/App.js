import Content from "./views/Content";

import '@fortawesome/fontawesome-free/css/all.min.css';

function App() {
  return (
    <div>
      <Content />
    </div>
  );
}

export default App;
