import { BUTTON_DETAIL_HANDLER, BUTTON_QUANTITY_HANDLER } from "../constants/order.constant";


const initialState = {
  mobileList: [
    {
      id: 1,
      name: "IPhone X",
      price: 900,
      quantity: 0
    },
    {
      id: 2,
      name: "Samsung S9",
      price: 800,
      quantity: 0
    },
    {
      id: 3,
      name: "Nokia 8",
      price: 650,
      quantity: 0
    }
  ],
  orderDetail: null,
  path: ""
}
const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case BUTTON_QUANTITY_HANDLER:
      state.mobileList[action.data].quantity++;
      break;
    case BUTTON_DETAIL_HANDLER:
      state.orderDetail = action.data;
      break;
    default:
      break;
  }
  return { ...state }
}
export default orderReducer;