import { Box, Button, Container, Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { onBtnOrderDetail, onBtnQuantilyHandler } from "../actions/order.action";
import routerList from "../routes";

const styleBox = {
    padding: "25px",
    margin: "10px",
    border: "none",
    borderRadius: "5px",
    lineHeight: "5px",
    boxShadow: "rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px",
}

const Order = () => {
    // B3: Khai báo dispatch để đẩy action tới reducer
    const dispatch = useDispatch();
    //B1: nhận giá trị khởi tạo của state trong giai đoạn mở đầu
    const { mobileList } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    });
    //set giá trị điều hướng url param
    const navigate = useNavigate();

    //tính tổng giá tiền đơn hàng dựa trên số lượng hàng đã chọn
    const total = mobileList.reduce((summary, item) => summary + item.price * item.quantity, 0);
    //B2:
    const onButtonBuyClickHandler = (value, id) => {
        console.log(value);
        dispatch(onBtnQuantilyHandler(id));
    }
    const onButtonDetailClickHandler = (value) => {
        console.log("Button Detail Clicked!");
        console.log(value);
        dispatch(onBtnOrderDetail(value));
        navigate("/detail")
    }
    return (
        <Container>
            <Grid container spacing={5} style={{ padding: "0px", marginTop: "200px" }}>
                {mobileList.map((value, index) => {
                    return (
                        <Box sx={{ flexGrow: 1 }} style={styleBox}>
                            <Grid container>
                                <Grid item xs={4} key={index}>
                                    <h4>{value.name}</h4>
                                    <p>{value.price}</p>
                                    <p >Quantity:{value.quantity}</p>
                                </Grid>
                                <Grid container spacing={2} paddingTop={3}>
                                    <Button style={{ backgroundColor: "#44bd32", color: "white" }} onClick={() => onButtonBuyClickHandler(value, index)}><i class="fa-solid fa-cart-shopping"></i>&nbsp;Buy</Button>
                                    <Button style={{ marginLeft: "10px", backgroundColor: "#0984e3", color: "white" }} onClick={() => onButtonDetailClickHandler(value, routerList.path)}><i class="fa-solid fa-info"></i>&nbsp;Detail</Button>
                                </Grid>
                            </Grid>
                        </Box>
                    )
                })}
            </Grid>
            <Grid>
                <h4>Total: {total} USD</h4>
            </Grid>
        </Container>

    )
}
export default Order;