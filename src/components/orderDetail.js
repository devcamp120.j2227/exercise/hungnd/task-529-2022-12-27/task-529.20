import { useSelector } from "react-redux"
import { Box, Button, Container, Grid } from "@mui/material";
import { useNavigate} from "react-router-dom";

const styleBox = {
    display: "block",
    padding: "25px", 
    margin: "0 auto",
    width: "300px",
    border: "none",
    borderRadius: "5px",
    lineHeight: "5px",
    boxShadow: "rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px",
}

const OrderDetail = () => {
    const { orderDetail } = useSelector((reduxData) =>
        reduxData.orderReducer
    )
    const navigate = useNavigate();

    const onButtonBackClickHandler = () => {
        navigate("/");    
    }
    return (
        <>
            {orderDetail !== null ?
                <Container>
                    <Box sx={{ flexGrow: 1 }} style={styleBox}>
                        <Grid container>
                            <Grid item>
                                <h4>{orderDetail.name}</h4>
                                <p>{orderDetail.price}</p>
                                <p >Quantity:{orderDetail.quantity}</p>
                            </Grid>
                            <Grid container spacing={2} style={{paddingTop: "30px", paddingLeft: "250px"}}>
                            <Button style={{ backgroundColor: "green", color: "white", width: "auto" }} onClick={() => onButtonBackClickHandler()}>Back</Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Container>
                : null}
        </>
    )
}
export default OrderDetail;